This is the source file I personally typed-in from the MPM-224 documentation. 
An effort was taken to preserve the style of the writing in comments, including
mistakes where found. 

The original source was written for an assembler that is no longer available.
The source file was translated into the format assembled using the _A18_ 1802
assembler written by William C. Colley, III.

Additions were made to for selection of choice for EF line and/or logic level
of the EF line and the Q output.

RCA MPM-24 documentation including UT4 source code published under fair use
exemption for research purposes. All rights reserved RCA.